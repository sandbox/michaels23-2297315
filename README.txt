// $Id: README.txt,v 1.1 2014/05/31 22:27:17 michaels23 Exp $

Readme file for the UC Roles Reminders module for Drupal
--------------------------------------------------------

The UC Roles Reminders module provides the ability to add multiple reminder triggers to UC Roles.

Installation:
  Installation is like with all normal drupal modules:
  Extract the 'uc_roles_reminders' folder from the tar ball to the modules directory
  for your installation (typically sites/all/modules).

Dependencies:
  The UC Roles Reminders module requires the UC Roles module.

Conflicts/known issues:

Configuration:
  The configuration section is added to admin/store/settings/products/edit/features. There, users with
  sufficient permissions can configure reminders as conditional actions predicates than be used 
  to schedule sending of e-mails to remind users of an expiring role.